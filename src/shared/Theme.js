export default {
  upVote: '#1CBBB4',
  downVote: '#FFAD1D',
  upVoteRGBA: '28, 187, 180',
  downVoteRGBA: '255, 173, 29'
};
