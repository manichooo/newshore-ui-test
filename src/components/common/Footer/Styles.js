import styled from 'styled-components';

export const Section = styled.footer`
  position: relative;
  background-color: #0A2463;
  padding: 40px;
  & a.text {
    font-size: 12px;
    color: white;
  }
`;
