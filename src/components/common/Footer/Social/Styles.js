import styled from 'styled-components';

export const H4 = styled.h4`
  font-size: 20px;
  color: white;
  font-weight: 400;
`;