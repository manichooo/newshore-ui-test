import React from "react";
import Icons from "./Icons";
import { Row, Col } from "@bootstrap-styled/v4";
import { H4 } from "./Styles";

function Social() {
  return (
    <React.Fragment>
      <Row>
        <Col md={12} className="d-flex justify-content-center">
          <H4>Follow Us</H4>
        </Col>
        <Col md={12} className="d-flex justify-content-center">
          <Icons />
        </Col>
      </Row>
    </React.Fragment>
  );
}

export default Social;
