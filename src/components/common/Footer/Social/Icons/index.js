import React from "react";
import Icon from "../../../../../shared/icomoon/icon";
import { Row, Col } from "@bootstrap-styled/v4";
import { NavLink } from "./Styles";

function Icons() {
  return (
    <React.Fragment>
      <Row>
        <Col md={12} className="d-flex align-items-center justify-content-center">
          <NavLink
            href="#"
            className="mr-3 d-flex align-items-center justify-content-center"
          >
            <Icon
              className="icon icon-facebook"
              icon="facebook"
              color="#fff"
              size="30px"
            />
          </NavLink>
          <NavLink
            href="#"
            className="mr-3 d-flex align-items-center justify-content-center"
          >
            <Icon
              className="icon icon-instagram"
              icon="instagram"
              color="#fff"
              size="30px"
            />
          </NavLink>
          <NavLink
            href="#"
            className="d-flex align-items-center justify-content-center"
          >
            <Icon
              className="icon icon-twitter"
              icon="twitter"
              color="#fff"
              size="30px"
            />
          </NavLink>
        </Col>
      </Row>
    </React.Fragment>
  );
}

export default Icons;
