import styled from 'styled-components';

export const NavLink = styled.a`
  border-radius: 50%;

  & .icon {
    transition: all 300ms ease-in-out;
  }
  
  & .icon-facebook:hover {
    fill: #3b5998 !important;
  }

  & .icon-twitter:hover {
    fill: #48c4d2 !important;
  }

  & .icon-instagram:hover {
    fill: #517FA4 !important;
  }
`;