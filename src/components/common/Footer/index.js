import React from 'react';
import Social from "../../common/Footer/Social"
import Links from "../../common/Footer/Links"
import {
  Row,
  Col  
} from '@bootstrap-styled/v4';
import {
  Section
} from './Styles';

function Footer() {
  return (
    <React.Fragment>
      <Section className="mx-auto py-4 mt-5">
        <Row>
          <Col md={12} className="d-flex align-items-center justify-content-center my-3">
            <Social />
          </Col>
          <Col md={12} className="d-flex align-items-center justify-content-center my-4">
            <Links />
          </Col>
        </Row>
      </Section>
    </React.Fragment>
  );
}

export default Footer;
