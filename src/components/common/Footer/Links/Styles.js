import styled from 'styled-components';

export const H4 = styled.h4`
  font-size: 18px;
  text-transform: uppercase;
  color: #FB3640;
  font-weight: 400;
`;

export const P = styled.p`
  font-size: 14px;  
  color: white;
  font-weight: 400;
`;

export const UL = styled.ul`
  list-style: none;
  padding: 0;
`;

export const LI = styled.li`
  margin: 5px 0;
  & a {
    color: white;
    font-size: 18px;
    font-weight: 400;
  }
`;
