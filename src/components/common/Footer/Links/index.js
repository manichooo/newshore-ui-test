import React from "react";
import { Row, Col, NavLink } from "@bootstrap-styled/v4";
import { H4, P, UL, LI } from "./Styles";

import demoData from "../../../../data/db.json";

function Links() {
  const linkItems = demoData.links.map((section, index) => {
    return (
      <Col key={index} lg={3}>
        <H4 className="justify-content-sm-center justify-content-md-start d-flex">{section.section}</H4>
        {section.desc_text ? (
          <P className="justify-content-sm-center justify-content-md-start d-flex">
            {section.desc_text}
          </P>
        ) : (
          ""
        )}
        {section.items.length > 0 ? (
          <UL>
            {section.items.map((item, index) => {
              return (
                <LI key={index} className="justify-content-sm-center justify-content-md-start d-flex">
                  <NavLink href={item.path}>{item.text}</NavLink>
                </LI>
              );
            })}
          </UL>
        ) : null}
      </Col>
    );
  });

  return (
    <React.Fragment>
      <Row className="d-flex align-items-center">{linkItems}</Row>
    </React.Fragment>
  );
}

export default Links;
