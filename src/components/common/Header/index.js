import {
  Section
} from './Styles';
import Menu from './Menu';
import Featured from './Featured';

/* Import Demo Deta */
import demoData from '../../../data/db.json';

function Header(props) {
  const { page } = props;
  const hero  = demoData.featured[page];

  return (
    <Section bg_image={process.env.PUBLIC_URL + `/images/${hero.file}`}>
      <Menu />
      <Featured data={hero} />
    </Section>
  );
  
}

export default Header;
