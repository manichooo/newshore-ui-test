import styled from "styled-components";

export const MenuWrapper = styled.div`
  z-index: 3;
  position: relative;
  max-width: 1024px;
  margin: 0 auto;
  & button.navbar-toggler {
    border: none;
    outline: none;
  }

  & .navbar {
    & .container {
      @media (max-width: 768px) {
        max-width: 768px;
      }

      @media (min-width: 768px) and (max-width: 991px) {
        max-width: 991px;
      }
    }
  }

  & div.navbar-collapse {
    @media (max-width: 991px) {
      position: absolute;
      width: 100%;
    }
  }
`;

export const NavBarWrapper = styled.div`
  & .navbar-nav {    
    .nav-item.active a, .nav-item a:hover {
      position: relative;
      &::before {
        content: "";
        left: 10%;
        bottom: 10%;
        width: 80%;
        height: 2px;
        position: absolute;
        background-color: white;
      }

      @media (max-width: 991px) {
        background-color: white;
        color: rgba(0, 0, 0, 0.75) !important;
      }
    }
  }
  @media (max-width: 991px) {
    background-color: rgba(0, 0, 0, 0.75);
  }
`;

export const NavBrandToggler = styled.div``;

export const TextBrand = styled.span`
  cursor: pointer;
  font-size: 32px;
`;
