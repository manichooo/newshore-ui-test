import React, { useState } from "react";
import Lang from "../Lang";
import { useLocation } from "react-router-dom";
import {
  A,
  Navbar,
  Container,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Media,
  Row,
  Col,
} from "@bootstrap-styled/v4";
import { MenuWrapper, NavBrandToggler, NavBarWrapper } from "./Styles";
import demoData from "../../../../data/db.json";

function Menu() {
  let location = useLocation();
  const [toogleMenu, setToogleMenu] = useState(false);

  //build top navigation links on header
  const navItems = demoData.navigation.map((item, index) => {
    const isActive = location.pathname === item.path;
    const navLinkClass = isActive ? "active" : "";

    return (
      <NavItem key={index} className={navLinkClass}>
        <NavLink className="text-white py-3 py-lg-2 px-4" href={item.path}>
          {item.nav_text}
        </NavLink>
      </NavItem>
    );
  });

  // update flag open/close menu on mobile devices
  const handleTogglerState = () => {
    setToogleMenu(!toogleMenu);
  };

  return (
    <React.Fragment>
      <Row>
        <Col md={12}>
          <Lang />
        </Col>
      </Row>
      <MenuWrapper className="MenuWrapper">
        <Navbar color="faded" className="p-0" light toggleable="md">
          <Container className="px-lg-0">
            <NavBrandToggler className="d-flex justify-content-between px-3 px-lg-0">
              <NavbarBrand className="p-0" tag={A} href="/">
                <Media
                  fluid
                  object
                  src={process.env.PUBLIC_URL + `/images/airlane_logo.png`}
                />
              </NavbarBrand>
              <NavbarToggler className="px-0" onClick={handleTogglerState} />
            </NavBrandToggler>
            <Collapse
              className="justify-content-end"
              navbar
              isOpen={toogleMenu}
            >
              <NavBarWrapper className="mx-3">
                <Nav navbar>{navItems}</Nav>
              </NavBarWrapper>
            </Collapse>
          </Container>
        </Navbar>
      </MenuWrapper>
    </React.Fragment>
  );
}

export default Menu;
