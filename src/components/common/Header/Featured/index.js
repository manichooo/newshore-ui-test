import { Row, Col, NavLink } from "@bootstrap-styled/v4";
import { Content, H3, H1, Link, Container } from "./Styles";

function Featured(props) {
  const { hero_title, hero_desc, cta_text } = props.data;

  return (
    <Container>
      <Row className="mx-3 mx-xl-0">
        <Col className="p-0" lg={7}>
          <Content>
            <H1>{hero_title}</H1>
            <H3>{hero_desc}</H3>
            {cta_text ? (
              <Link className="d-flex align-items-center my-4">
                <NavLink href="/">{cta_text}</NavLink>
              </Link>
            ) : (
              ""
            )}
          </Content>
        </Col>
      </Row>
    </Container>
  );
}

export default Featured;
