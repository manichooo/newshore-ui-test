import styled from 'styled-components';

export const Container = styled.div`
  max-width: 1024px;
  margin: 0 auto;
  padding-top: 40px;
  position: relative;
  z-index: 1;
`;

export const Content = styled.div`
  padding: 40px 0;
  color: white;
  line-height: normal;
`;

export const H3 = styled.h3`
  font-weight: 300;  
`;

export const H1 = styled.h1`
  font-weight: bold;  
`;

export const Link = styled.div`
  line-hieght: normal;
  & a {
    color: white !important;
    font-size: 20px;
    line-height: normal;
    font-weight: bold;
    background-color: #FB3640;
    border-radius: 30px;
    padding: 10px 68px;
    text-transform: uppercase;
  }
`;
