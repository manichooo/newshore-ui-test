import styled from 'styled-components';

export const Section = styled.header`
  padding: 10px 25px 25px 25px;
  overflow: hidden;
  position: relative;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top center;
  background-image: url(${props => props.bg_image});
  @media (min-width: 992px) {
    min-height: 475px;
  }
`;
