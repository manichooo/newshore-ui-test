import React, { useState } from "react";
import Icon from "../../../../shared/icomoon/icon";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "@bootstrap-styled/v4";

import { LangWrapper } from "./Styles";

function Lang() {
  const [lang, setLang] = useState(false);

  // update flag open/close menu on mobile devices
  const handlerLangState = () => {
    setLang(!lang);
  };
  return (
    <LangWrapper className="d-flex justify-content-end pr-md-0 pr-lg-4">
      <Dropdown
        isOpen={lang}
        toggle={handlerLangState}
      >
        <DropdownToggle className="d-flex align-items-center justify-content-center drp-toggle">
          en
          <Icon icon="arrow-down" color="#fff" size="16px" />
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem>RU - Russian</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>DE - German</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>FR - French</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </LangWrapper>
  );
}

export default Lang;
