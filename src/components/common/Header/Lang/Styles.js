import styled from "styled-components";

export const LangWrapper = styled.div`
  z-index: 4;
  position: relative;
  max-width: 1024px;
  margin: 0 auto;
  & .dropdown, & .dropdown.show {
    & button.drp-toggle { padding: 0 15px 0 0; }
    & button, button:focus, button:hover {
      outline:0;
      background: none;
      border: none;
      box-shadow: none;
    }

    .dropdown-menu {
      @media (max-width: 1220px) {
        left: -127px;
      }
    }
  }
`;
