import React from "react";
import { Row, Col, NavLink } from "@bootstrap-styled/v4";

import {
  Section,
  Card,
  CardBlock,
  CardTitle,
  CardSubtitle,
  Media,
  Title,
} from "./Styles";
import demoData from "../../../data/db.json";

function Offers() {
  // build card offers from demoData file
  const offers = demoData.offers.map((offer, index) => {
    return (
      <Col
        key={index}
        lg={4}
        className="d-flex align-items-center justify-content-center py-3"
      >
        <NavLink>
          <Card>
            <Media
              fluid
              object
              src={process.env.PUBLIC_URL + `/images/${offer.file}`}
            />
            <CardBlock>
              <CardTitle>{offer.title}</CardTitle>
              <CardSubtitle>Price: {offer.price}</CardSubtitle>
            </CardBlock>
          </Card>
        </NavLink>
      </Col>
    );
  });

  return (
    <React.Fragment>
      <Section className="mx-3 mx-xl-auto py-4 mt-5">
        <Row className="mx-0 my-3">
          <Col>
            <Title md={12}>Offers</Title>
          </Col>
        </Row>
        <Row className="mx-0">{offers}</Row>
      </Section>
    </React.Fragment>
  );
}

export default Offers;
