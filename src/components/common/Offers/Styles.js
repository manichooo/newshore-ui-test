import styled from "styled-components";

export const Section = styled.section`
  position: relative;
  max-width: 1024px;
`;

export const Card = styled.span`
  display: flex;
  align-items: end;
  border-radius: 20px;
  position: relative;
  overflow: hidden;
  box-shadow: 2px 2px 10px rgba(10, 36, 99, 0.4);
  border: 1px solid #0a2463;
`;

export const CardTitle = styled.p`
  color: white;
  font-size: 16px;
  margin: 15px 0 0 15px;
`;

export const CardSubtitle = styled.p`
  color: #247ba0;
  font-size: 21px;
  font-weight: bold;
  margin: 0 10px 0 0;
  text-align: right;
`;

export const CardBlock = styled.span`
  height: 50%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.68);
  position: absolute;
`;

export const Media = styled.img`
  position: relative;
  transition: all 300ms ease-in-out;
  width: 100%;
  height: auto;
  &:hover {
    transform: scale(1.1);
  }
`;

export const Title = styled.div`
  color: #247ba0;
  font-size: 26px;
  font-weight: 400;
  &::before {
    content: "";
    right: 2%;
    top: 50%;
    width: 88%;
    height: 1px;
    position: absolute;
    background-color: #605F5E;
  }
`;
