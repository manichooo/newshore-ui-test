import React from "react";
import Header from "../../common/Header";
import Footer from "../../common/Footer";

function Contact() {
  return (
    <React.Fragment>
      <Header page={3} />
      <Footer />
    </React.Fragment>
  );
}

export default Contact;
