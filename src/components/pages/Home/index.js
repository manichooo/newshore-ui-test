import React from "react";
import Header from "../../common/Header";
import Offers from "../../common/Offers";
import Footer from "../../common/Footer";

function Home() {
  return (
    <React.Fragment>
      <Header page={0} />
      <Offers />
      <Footer />
    </React.Fragment>
  );
}

export default Home;
