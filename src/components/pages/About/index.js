import React from "react";
import Header from "../../common/Header";
import Footer from "../../common/Footer";

function About() {
  return (
    <React.Fragment>
      <Header page={1} />
      <Footer />
    </React.Fragment>
  );
}

export default About;
