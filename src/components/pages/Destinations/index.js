import React from "react";
import Header from "../../common/Header";
import Footer from "../../common/Footer";

function Destinations() {
  return (
    <React.Fragment>
      <Header page={2} />
      <Footer />
    </React.Fragment>
  );
}

export default Destinations;
