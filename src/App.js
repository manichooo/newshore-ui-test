import React from 'react';
import BootstrapProvider from '@bootstrap-styled/provider';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

//Import App Components
import Home from './components/pages/Home';
import About from './components/pages/About';
import Destinations from './components/pages/Destinations';
import Contact from './components/pages/Contact';

const Wrapper = ({ theme }) => (
  <BootstrapProvider theme={theme}>
    <BrowserRouter>
      <Routes>
        <Route path="/" exact element={<Home />}/>
        <Route path="about" element={<About />} />
        <Route path="destinations" element={<Destinations />} />
        <Route path="contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  </BootstrapProvider>
);

function App() {
  return (
    <Wrapper theme={{
      '$font-family-base': `'Roboto', sans-serif`
    }} />
  );
}

export default App;
