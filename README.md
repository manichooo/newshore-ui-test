This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Layout HTML-CSS
For the layout HTML-CSS i used the following:

- [x] [Bootstrap Styled](https://bootstrap-styled.github.io/v4/#/Introduction) NPM module for React UI development.
- [x] [React Styled Components](https://www.styled-components.com) to style the layout using ES6 and CSS3 features.
- [x] Use of [Media Queries](https://www.w3schools.com/css/css_rwd_mediaqueries.asp) to guarantee the correct vizualization of views over diferents display resolutions and mobile devices.
- [x] Clicking over links of the main top redirects the user to other pages with Demo data provided in the .PSD comps.

### Interaction - JS
For the Interaction i used the following:

- [x] [React JS](https://en.reactjs.org/) a JavaScript library for building user interfaces, to build all the funcionality and achieve the requirements of the test.
- [x] Use of a local db.json file with [JSON](https://www.json.org/json-en.html) format with the structure for content and for and populate the initial content to show in UI.

### Deploy
For the Deploy process i used the following:

- [x] [The Netlify Platform](https://www.netlify.com/products) to easy deploy modern web apps from local setup to global deployment.

### Run locally
To run the web app on local enviorment follow the nexts steps.

- [x] Clone this repository typing or paste the following in your terminal or commands console `git clone https://manichooo@bitbucket.org/manichooo/newshore-ui-test.git`
- [x] Enter to the cloned folder or directory with the command: `cd newshore-ui-test` in your teminal or commands console.
- [x] Type and run `npm install` to install all the necessary dependencies.
- [x] Thereafter you are be able to type and run `npm start` to see the live app at `http://localhost:3000`.

